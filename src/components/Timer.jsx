import React from 'react'

export default class Timer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            second: 0
        }

    }

    triggerStart = () => {
        this.interval = setInterval(() => {
            this.setState({
                second: this.state.second + 1
            })
        }, 1000);
    }

    componentDidMount() {
        this.triggerStart()
    }

    triggerStop = () => {
        clearInterval(this.interval)
    }

    render() {
        return (
            <div className="jumbotron text-center align-middle ">
                <h1 className="display-4">Timer: {this.state.second}</h1>
                <button className="btn btn-primary m-1" onClick={this.triggerStart}>Start</button>
                <button className="btn btn-danger m-1" onClick={this.triggerStop}>Stop</button>
            </div>
        )
    }


}